# EndomorphismsThroughIsogenies

Implementation of algorithms that provides the description of endomorphism rings
of supersingular elliptic curves through the knowledge of an isogeny from a
particular curve.

## Library requirements
Magma    v2.24-2  (http://magma.maths.usyd.edu.au/magma/)

The codes and scripts are not guaranteed to work for another (older) version.

## How to run
To run a (toy) example of conversion between a connecting ideal and an isogeny,
execute:
```
magma ./example/isogeny-to-ideal.m
magma ./example/ideal-to-isogeny.m
```

To run an example of equivalent ideal computation, execute:
```
magma ./example/equivalent-ideal.m
```

*(not currently available)* To run the tests, execute:
```
magma ./test/Test<file.m>
```
where `<file.m>` is a file of `./src/`.


## Structure of the repository
* `./src` contains magma files corresponding to the arithmetic of the different
structures involved (modules, quaternion algebras, and elliptic curves).
* `./test` contains tests for verifying the computations *(not currently
  available)*.
* `./example` contains instances of examples that compute the correspondence
between ideal and isogeny, and equivalent ideals as in [AC:GPST16].
