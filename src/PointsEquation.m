/*
 Suppose that R \in E[\ell] = \langle P, Q\rangle,
 find integers u and v such that R = u * P + v * Q
*/

// hardcoded cyclotomic polynomial evaluation for the case ell = 2^n
function EvalCycloPoly(ell, x)
    /*
    INPUT:
    * ell an integer
    * x an element of a finite field
    OUTPUT:
    * the ell-th cyclotomic polynomial evaluated at x
    */
    n2 := Integers()!Floor(Log(2,ell));
    if ell eq 2^n2 then
	y := x;
	for i in [1..n2-1] do
	    y := y^2;
	end for;
	return y + 1;
    else
	Phi := CyclotomicPolynomial(ell);
	return Evaluate(Phi, x);
    end if;
end function;

function TorsionBasis(E, ell)
    /*
    INPUT:
    * E an elliptic curve
    * ell an integer
    OUTPUT:
    * a basis of E[ell]
    */
    p := Characteristic(BaseField(E));
    assert (p+1) mod ell eq 0;
    DivisorsEll := Divisors(ell); // expensive
    DivisorsEll := DivisorsEll[2..#DivisorsEll-1];
    C<x> := PolynomialRing(Integers());

    // for the moment it works only if FactoEll has one element
    repeat
	P := ((p+1) div (ell)) * Random(E);
    until not(IsZero(P)) and IsZero(ell*P) and &and([not(IsZero(d *P)) : d in DivisorsEll]);
    repeat
	Q := ((p+1) div (ell)) * Random(E);
    until not(IsZero(Q)) and IsZero(ell*Q) and &and([not(IsZero(d*Q)) : d in DivisorsEll]) and EvalCycloPoly(ell, WeilPairing(P, Q, ell)) eq 0;
    return P, Q;
end function;

function InBasePrimePower(R, P, Q, ell, e)
    /*
    INPUT:
    * R a point of E[ell^e]
    * P a point of E[ell^e]
    * Q a point of E[ell^e] such that <P,Q> = E[ell^e]
    * ell a prime
    * e an integer
    OUTPUT:
    * n1 and n2 such that n1*P + n2*Q = R
    */
    E := Curve(R);
    w := WeilPairing(P, Q, ell^e);
    L:=[w];
    wx:=w;
    for i in [1..e-1] do wx:=wx^ell; Append(~L,wx); end for;
    bs:={@ @};
    g:=Parent(wx)!1;
    C:=Ceiling(Sqrt(ell));
    for i in [0..C-1] do
        Include(~bs,g);
        g:=g*wx;
    end for;
    g:=g^-1;
    DL:=function(l)
        /* costs sqrt(ell) operations in the finite field. */
        for i in [0..C-1] do
            j:=Index(bs,l);
            if j ne 0 then return i*C+j-1; break; end if;
            l*:=g;
        end for;
        assert false;
    end function;
    DLx:=function(l)
        n:=0;
        for s in [0..e-1] do
            t:=DL(l^(ell^(e-1-s)));
            l /:= L[1+s]^t;
            n +:= t * ell^s;
        end for;
        return n;
    end function;
    l:=WeilPairing(P,R,ell^e); // w^nQ
    nQ:=DLx(l);
    assert l eq w^nQ;
    l:=WeilPairing(R,Q,ell^e); // w^nP
    nP:=DLx(l);
    assert l eq w^nP;
    assert R eq nP*P+nQ*Q;
    return nP,nQ;
end function;

function InBase(R, P, Q, N)
    /*
    INPUT:
    * R a point of order N
    * P a point of order N
    * Q a point of order N such that <P, Q> = E[N]
    * N an integer
    OUTPUT:
    * two integers (mod N) such that R = n1 * P + n2 * Q
    */
    E := Curve(R);
    Facto := Factorization(N);
    L := [];
    Decomposition := [];
    for fe in Facto do
	f := fe[1]^fe[2];
	cof := N div f;
	fR := cof * R;
	fP := cof * P;
	fQ := cof * Q;
	u,v := InBasePrimePower(fR, fP, fQ, fe[1], fe[2]);
	Append(~Decomposition, [u,v,f]);
    end for;
    if #Decomposition eq 1 then
	assert #Facto eq 1;
	return Decomposition[1][1], Decomposition[1][2];
    end if;
    // CRT
    common_div, Bezout_coeffs := Xgcd([x[3] : x in Eltseq(Decomposition)]);
    Sol := Vector(Integers(), [0,0]);
    for j in [1..#Decomposition] do
	Sol +:= &+([Bezout_coeffs[i] * Decomposition[i][3] : i in [1..#Bezout_coeffs] | i ne j]) * Vector(Decomposition[j][1..2]);
    end for;
    return Sol[1], Sol[2];
end function;

function SolveEq(L : RHS := 0, Torsion := 0)
    /*
    INPUT:
    * L a list of points of an elliptic curve
    OPTIONAL INPUT:
    * RHS another point of the elliptic curve
    * Torsion the order of all the points in input
    OUTPUT:
    * a list of integers n_i such that \sum_i n_i * L[i] = RHS
    */
    if Torsion eq 0 then
	N := Lcm([Order(P) : P in L]);
    else
	N := Torsion;
    end if;
    P, Q := TorsionBasis(Curve(L[1]), N);
    M := Matrix(Integers(N), #L, 2, []);
    for i in [1..#L] do
	u, v := InBase(L[i], P, Q, N);
	M[i][1] := u;
	M[i][2] := v;
    end for;
    if IsZero(RHS) then
	H, T := HermiteForm(M);
	i := 1;
	// the first row of T that brings 0 in the HNF
	while Eltseq(H[i]) ne [Integers(N)!0,Integers(N)!0] do
	    i +:= 1;
	end while;
	if i gt #L then
	    return [];
	else
	    return T[i];
	end if;
    else
	// Non homogeneous equation is a bit tricky
	M2 := Matrix(Integers(N), 1, 2, []);
	u, v := InBase(RHS, P, Q, N);
	M2[1][1] := u;
	M2[1][2] := v;
	M := VerticalJoin(M, M2);
	H, T := HermiteForm(M);
	H2, T2 := HermiteForm(Matrix([[T[i,#L+1]]:i in [1+Rank(H)..Nrows(M)+1]]));
	return -T2[1]*Matrix(T[1+Rank(H)..Nrows(M)+1]);
    end if;
end function;
