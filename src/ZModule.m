function RationalHNF(M)
    /*
    INPUT:
    * a matrix with coefficeints in QQ
    OUPUT:
    * the HNF of the matrix M, and the transformation matrix (defined over ZZ)
    */
    d := Lcm([Denominator(x) : x in Eltseq(M)]);
    H, T := HermiteForm(ChangeRing(d*M, Integers()));
    return 1/d * ChangeRing(H, Rationals()), T;
end function;

function IntersectionZModules(L)
    /*
    INPUT:
    * L a list of matrices representing lattices (each row is a vector of the
      basis.
    OUPUT:
    * a matrice representing the intersection lattice (each row is a vector
      of the basis.
    */
    if #L eq 1 then
        return L[1];
    elif #L eq 2 then
	s := VerticalJoin(L[1],L[2]);
	_,T:=RationalHNF(s);
	K := RowSubmatrix(T, Ncols(L[1])+1, Nrows(L[1]) + Nrows(L[2]) - Ncols(L[1]));
	return ColumnSubmatrix(K, NumberOfColumns(L[1])) * L[1];
    else
        w := IntersectionZModules(L[1..2]);
        return IntersectionZModules([w] cat L[3..#L]);
    end if;
end function;

function RandomZModuleElt(L: bound := 10)
    /*
    INPUT:
    * L a Z-module
    OPTIONAL INPUT:
    * bound a bound on the random coefficients
    OUTPUT:
    * a random element of the Z-module
    */
    return &+([Random(0,bound) * r : r in Rows(L)]);
end function;

function In(x,L)
    /*
    INPUT:
    * x a vector
    * L a Z-module
    OUTPUT:
    * true/false if x is in the Z-module L
    */
    return &and([Denominator(t) eq 1 : t in Eltseq(x*L^-1)]);
end function;
