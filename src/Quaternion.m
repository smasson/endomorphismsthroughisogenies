
/*
i² = -q
j² = -p

The frobenius is j.
*/

Rationalijk<i,j,k>:=PolynomialRing(Rationals(),3);
M:=Matrix(Rationalijk,4,4,[
    1,   i,    j,    k,
    i,  -q,    k, -q*j,
    j,  -k,   -p,  p*i,
    k, q*j, -p*i, -q*p]);
coeff_1:=map<Rationalijk->Rationals()|f:->Evaluate(f,[0,0,0])>;
coeff_i:=map<Rationalijk->Rationals()|f:->Evaluate(f,[1,0,0])-coeff_1(f)>;
coeff_j:=map<Rationalijk->Rationals()|f:->Evaluate(f,[0,1,0])-coeff_1(f)>;
coeff_k:=map<Rationalijk->Rationals()|f:->Evaluate(f,[0,0,1])-coeff_1(f)>;
mmap:=func<M,k|Matrix(Nrows(M),Ncols(M),[k(x):x in Eltseq(M)])>;
M1:=mmap(M, coeff_1);
Mi:=mmap(M, coeff_i);
Mj:=mmap(M, coeff_j);
Mk:=mmap(M, coeff_k);

function Multiply(A, B)
    /*
    INPUT:
    * A a list of four rationals representing a quaternion in the basis 1,i,j,k
    * B a list of four rationals representing a quaternion in the basis 1,i,j,k
    OUTPUT:
    * a list of four rationals representing the quaternion A*B in the basis 1,i,
      j, k
    */
    return  Vector([
    (A*ChangeRing(M1, Parent(A[1])), B),
    (A*ChangeRing(Mi, Parent(A[2])), B),
    (A*ChangeRing(Mj, Parent(A[3])), B),
    (A*ChangeRing(Mk, Parent(A[4])), B)]);
end function;

function Conjugate(A)
    /*
    INPUT: 
    * A a list of four rationals representing a quaternion in the basis 1,i,j,k
    OUTPUT:
    * a list of four rationals representing the conjugate of A
    */
    return Vector([A[1], -A[2], -A[3], -A[4]]);
end function;

function Norm(A)
    /*
    INPUT: 
    * A a list of four rationals representing a quaternion in the basis 1,i,j,k
    OUTPUT:
    * the norm of the quaternion A
    */
    n := Multiply(A, Conjugate(A));
    assert n[2] eq 0 and n[3] eq 0 and n[4] eq 0;
    return n[1];
end function;

function Inverse(A)
    /*
    INPUT: 
    * A a list of four rationals representing a quaternion in the basis 1,i,j,k
    OUTPUT:
    * the inverse of the quaternion A
    */
    return Conjugate(A) / Norm(A);
end function;
