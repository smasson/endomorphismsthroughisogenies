// Convention: I is a left-ideal of O if O is a left order (i.e. OI subset I)

function Evaluate_i(P)
    /*
    INPUT:
    * P=(x,y) a point of the elliptic curve j=0 or 1728
    OUTPUT:
    * the point (zeta*x, y) (resp. (-x,iy)) if j=0 (resp. if j=1728)
    */
    jInv := jInvariant(Curve(P));
    Fp2 := Parent(jInv);
    Fp2y<y> := PolynomialRing(Fp2);
    if jInv eq 1728 then
	return Curve(P)![-P[1], Generator(Fp2) * P[2]];
    else if jInv eq 0 then
	    zeta := Roots(ChangeRing(CyclotomicPolynomial(3), Fp2y))[1][1];
	    return Curve(P)![zeta * P[1], P[2]];
	else
	    print "no known i endomorphism here...";
	    return false;
	end if;
    end if;
end function;

function Evaluate_j(P)
    /*
    INPUT:
    * P a point of an elliptic curve defined over Fp
    OUTPUT:
    * the point pi(P) where pi is the Frobenius map
    */
    return Curve(P)![P[1]^p, P[2]^p];
end function;

function EvaluateQuaternion(v, P : ListOfIsogenies := [],\
                            ListOfDualIsogenies := [], OrderP := 0)
    /*
    INPUT:
    * v is a quaternion given with four coordinates in the basis of the quaternion algebra
    * P is a point of a given curve
    OPTIONAL INPUT:
    * ListOfIsogenies is the list of isogenies from the j=1728 curve
    * ListOfDualIsogenies is the list of the duals of ListOfIsogenies
    * the order of the point P
    OUTPUT:
    * the point v(P) where v is seen as an endomorphism of Curve(P)
    */
    if OrderP eq 0 then
	OrderP := Order(P);
    end if;
    n := #ListOfIsogenies;
    if n eq 0 then
	if jInvariant(Curve(P)) eq 1728 then
	    d := 2;
	else if jInvariant(Curve(P)) eq 0 then
		d := 6;
	    else
		print "Curve unknown for the moment...";
		return false;
	    end if;
	end if;
	Q := _DivisionPoint(P, d : OrderP := OrderP);
        w := ChangeRing(d * Vector(v), Integers());
	R := w[1] * Q\
	     + w[2] * Evaluate_i(Q)\
	     + w[3] * Evaluate_j(Q)\
	     + w[4] * Evaluate_i(Evaluate_j(Q));
	return R;
    else
	Phi := ListOfIsogenies[n];
	HatPhi := ListOfDualIsogenies[n];
	N := Degree(Phi);
	Q := _DivisionPoint(P, N: OrderP := OrderP);
	w := N * Vector(v);
	Ord := OrderP;
	if Ord mod N eq 0 then
	    Ord := Ord div N;
	    // not sure...
	end if;
	if n gt 1 then
	    return Phi(EvaluateQuaternion(w, HatPhi(P) : ListOfIsogenies := ListOfIsogenies[1..n-1], ListOfDualIsogenies:=ListOfDualIsogenies[1..n-1], OrderP := Ord));
	else
    	    return Phi(EvaluateQuaternion(w, HatPhi(P) : OrderP := Ord));
	end if;
    end if;
end function;

function Discriminant(O)
    /*
    INPUT:
    * An order O
    OUTPUT:
    * the discriminant of the order O
    */
    D := Determinant(2*O*M1*Transpose(O));
    assert Denominator(D) eq 1;
    return Integers()!D;
end function;

function IdealMultiplication(L)
    /*
    INPUT:
    * L a list of ideals given by matrices
    OUTPUT:
    * the matrice representing the product of the ideals of L
    */
    if #L eq 1 then
	return L[1];
    elif #L eq 2 then
	I1 := L[1];
	I2 := L[2];
	
	M := Matrix([Multiply(i1, i2) : i1 in Rows(I1), i2 in Rows(I2)]);
	HNF_M, T := RationalHNF(M);
	assert RowSubmatrix(HNF_M, 5, 4) eq 0;
	return RowSubmatrix(HNF_M, 1, 4);
    else
	w := IdealMultiplication(L[1..2]);
	return IdealMultiplication([w] cat L[3..#L]);
    end if;
end function;

function IdealMultiple(I, alpha: side:= "right")
    /*
    INPUT:
    * I an ideal given by a matrix
    * alpha a quaternion
    OPTIONAL INPUT:
    * side in order to multiply by alpha on the left or right of I
    OUTPUT:
    * the matrix of the ideal I * alpha or alpha * I
    */
    if side eq "right" then
        return Matrix([Multiply(r,alpha): r in Rows(I)]);
    else
	return Matrix([Multiply(alpha, r) : r in Rows(I)]);
    end if;
end function;

function IdealSecondGenerator(I, N: m := Floor(Log(2,p)) + 1)
    /*
    INPUT:
    * I an ideal of O given by a matrix
    * N the norm of th ideal I
    OPTIONAL INPUT:
    * m a bound for the randomization
    OUTPUT:
    * an element alpha of I such that I = O * N + O * alpha
    */
    
    HNFI, _ := RationalHNF(I);
    repeat
	a := Random(1,m);
	b := Random(1,m);
	c := Random(1,m);
	d := Random(1,m);
	alpha := Vector([Rationals()!a,b,c,d]) * HNFI;
    until Gcd(Integers()!Norm(alpha), N^2) eq N;
    // this is just a test, remove it !
    //M1 := Matrix([N * b : b in Rows(O0)]);
    //M2 := Matrix([Multiply(b,alpha) : b in Rows(O0)]);
    //HNFM, _ := RationalHNF(VerticalJoin(M1,M2));
    //for i in [1..4] do
    //    u := Rows(HNFM)[i];
    //    assert In(u, Iprime);
    //end for;
    //assert Integers()!IdealNorm(RowSubmatrix(HNFM, 4), O0) eq N;
    //assert RationalHNF(Iprime) eq RowSubmatrix(HNFM, 4);
    return alpha;
end function;

function GetIdealEndomorphism(P, O: ListOfIsogenies := [], ListOfDualIsogenies := [], OrderP := 0)
    /*
    INPUT:
    * P is a point of a given curve
    * O a maximal order given by a matrix, representing End(Curve(P))
    OPTIONAL INPUT:
    * ListOfIsogenies is the list of isogenies from the j=1728 curve
    * ListOfDualIsogenies is the list of the duals of ListOfIsogenies
    * the order of the point P
    OUTPUT:
    * an endomorphism alpha of Curve(P) such that alpha(P) = 0
    */
    if OrderP eq 0 then
	ell := Order(P);
    else
	ell := OrderP;
    end if;

    // On résoud l'équation avec des points de l^e torsion. On n'aura besoin
    // que de 3 points car E[l^e] est un espace vectoriel de dimension 2.
    Basis_on_P := [];
    for l in [[1,0,0,0],[0,1,0,0], [0,0,1,0]] do
        alpha := ChangeRing(Vector(l), Rationals());
        im := EvaluateQuaternion(alpha * O, P\
              : ListOfIsogenies := ListOfIsogenies, \
				   ListOfDualIsogenies := ListOfDualIsogenies);
        Append(~Basis_on_P, im);
    end for;
    S := SolveEq(Basis_on_P);
    if Vector(S) eq Vector([0 : i in [1..#Basis_on_P]]) then
        print "There is a problem !";
    end if;
    return ChangeRing(Vector(Integers(), Eltseq(S) cat [0]),Rationals())*O;
end function;

function GetIdeal(P, alpha, O, side : OrderP := 0)
    /*
    INPUT:
    * P is the point defining the isogeny kernel
    * alpha an endomorphism in the basis of O, alpha(P) = 0
    * O the maximal order corresponding to End(Curve(P))
    * side depending on whether if we want a left or right ideal
    OPTIONAL INPUT:
    * OrderP the order of P
    OUTPUT:
    * a matrix representing an integral ideal of O, connecting O and the maximal order of the curve isogenous to Curve(P), of kernel <P>
    */
    if OrderP eq 0 then
	ell := Order(P);
    else
	ell := OrderP;
    end if;
    M := ell*O;
    if side eq "left" then
        N := Matrix(Rationals(),4, 4, [Multiply(b, Vector(alpha))\
                                       : b in Rows(O)]);
    elif side eq "right" then
        N := Matrix(Rationals(),4, 4, [Multiply(Vector(alpha), b)\
                              : b in Rows(O)]);
    end if;
    HNF_MN, T := RationalHNF(VerticalJoin(M,N));
    assert RowSubmatrix(HNF_MN, 5, 4) eq 0;
    Ideal := RowSubmatrix(HNF_MN, 4);
    return Ideal;
end function;

function ComputeOrder(I, side)
    /*
    INPUT:
    * I an ideal given by a matrix (each row is a quaternion of the basis)
    * side a string correspond to the order we want ("left" or "right")
    OUTPUT:
    * The matrix of the order (each row is a quaternion of the basis).
    REMARK:
    If B is the quaternion algebra and I = <u1,u2,u3,u4>, the left order of I
    is the intersection of the A_i := {x in B s.t. x \in I / u_i} for i in
    [1..4].
    */
    A := [];
    for i in [1..4] do
        ni := Norm(I[i]);
        assert ni ne 0;
        // We need to divide by ui to get A_i
        Inverse_ui := (1/ni) * Conjugate(I[i]);
        // Ai is the matrix whose rows are the uj/ui (j in [1..4])
        if side eq "left" then
            Ai := Matrix(Rationals(), 4, 4, [Multiply(uj, Inverse_ui): uj in Rows(I)]);
        elif side eq "right" then
            Ai := Matrix(Rationals(), 4, 4, [Multiply(Inverse_ui, uj): uj in Rows(I)]);
        end if;
        Append(~A, Ai);
    end for;
    Intersection := IntersectionZModules(A);
    assert Discriminant(Intersection) eq -p^2;
    return Intersection;
end function;

function ConjugateIdeal(I)
    /*
    INPUT:
    * I an ideal given by a matrix
    OUTPUT:
    * the ideal of the conjugates of the elements of I
    */
    return Matrix(Rationals(), 4, 4, [Conjugate(u) :  u in Rows(I)]);
end function;

function IdealNorm(I, O)
    /*
    INPUT:
    * I an ideal given by a matrix
    * O the order containing I, given by a matrix
    OUTPUT:
    * the norm of the I as an ideal of O (N(I) = [O:I]^(1/2))
    */
    NormSquared := Integers()!Determinant(I*O^-1);
    FactorsNormSquared := Factorization(NormSquared);
    for fe in FactorsNormSquared do
        assert fe[2] mod 2 eq 0;
    end for;
    return Integers()!&*([fe[1]^(fe[2] div 2) : fe in FactorsNormSquared]);
end function;

function GetIsogeny(I, E)
    /*
    INPUT:
    * I an ideal given by a matrix
    * E an elliptic curve such that I is an O ideal (O = End(E))
    OUTPUT:
    * the isogeny of kernel E[I]
    */
    O := ComputeOrder(I, "left");
    normI := IdealNorm(I, O);
    alpha := IdealSecondGenerator(I, normI);
    A, Aprime := TorsionBasis(E, normI);
    s := SolveEq([EvaluateQuaternion(alpha, A), EvaluateQuaternion(alpha,Aprime)]);
    return Integers()!s[1]*A+Integers()!s[2]*Aprime;
end function;

function GetIdealFromOrders(O1, O2)
    /*
    INPUT:
    * O1 an order
    * O2 an order
    OUTPUT:
    * the ideal O1 * O2
    */
    return IdealMultiplication([O1, O2]);
end function;

function GetMatrixEndoMod(a, r, F)
    /*
    INPUT:
    * an endomorphism a of an order O
    * r a square root of -1 in F
    * F a field of the form Z/NZ (N prime)
    OUTPUT:
    * the matrix representing a in O/NO
    */
    return Matrix(2,2,\
		  [F!(a[1] + a[2]*r) ,\
	       -p*(F!(a[3] + a[4]*r)),\
		   F!(a[3] - a[4]*r) ,\
		   F!(a[1] - a[2]*r)  \
		   ]);
end function;

function GetQuaternionMod(M, r)
    /*
    INPUT:
    * M a matrix representing an endomorphism of O/NO
    * r a square root of -1 in Z/NZ
    OUTPUT:
    * an endomorphism alpha of O (such that GetMatrixEndoMod(alpha, r, Z/NZ)=M)
    */
    return Vector([(M[1][1] + M[2][2])/2,
		   (M[1][1] - M[2][2])/(2*r),
		   (M[1][2] - p *M[2][1])/2,
		   (M[1][2] + p *M[2][1])/(2*r)
		   ]);
end function;

function PrimeNormEquivalentIdeal(I,O0: m := Floor(Log(2, p)) + 1)
    /*
    INPUT:
    * I an O ideal given by a matrix
    * O an order
    OPTIONAL INPUT:
    * m a bound for the randomization
    OUTPUT:
    * an ideal Iprime equivalent to I, of prime norm
    * an element delta such that Iprime = I * Conjugate(delta)/Norm(I)
    */
    // This could be done using the Gram matrix
    GramMat := Matrix(Rationals(), 4, 4, [0 : i in [1..16]]);
    for i in [1..4] do
	u := Eltseq(Rows(I)[i]);
	for j in [1..2] do
	    GramMat[i][j] := u[j];
	end for;
	for j in [3..4] do
	    GramMat[i][j] := Floor(Sqrt(p)) * u[j];
	end for;
    end for;
    A,B := HKZ(GramMat);
    NormI := Integers()!IdealNorm(I, O0);
    HKZ_I := B*I;
    repeat
	a := Random(1, m);
	b := Random(1, m);
	c := Random(1, m);
	d := Random(1, m);
	delta := Vector([Rationals()!a,b,c,d]) * HKZ_I;
	N := Integers()!Norm(delta) div (NormI);
    until IsPrime(N) and LegendreSymbol(-1, N) eq 1; // for simplicity!
    Iprime := IdealMultiple(I, Conjugate(delta)/NormI);
    return Iprime, delta;
end function;

function FindQuaternionOfNorm(n, p)
    /*
    INPUT:
    * n an integer
    * p the prime defining the quaternion algebra H_{-q, -p}
    OUTPUT:
    * an element of Z<1,i,j,k> of norm n
    */
    m := Floor(Sqrt(n div (2*p)));
    repeat
	c := Random(1,m);
	d := Random(1,m);
	rhs := n - p*(c^2+d^2);
    until IsPrime(rhs) and rhs mod 4 eq 1;
    bool, a, b := NormEquation(1,rhs);
    return Vector([Rationals()!a,b,c,d]);
end function;

function ApproximationInIdeal(N, beta1, alpha)
    /*
    INPUT:
    * N a prime integer corresponding to the norm of an ideal I
    * beta1 a quaternion not in I ? TODO
    * alpha an element of I such that I = O * N + O * alpha
    OUTPUT:
    * an element beta2 such that beta1*beta2 in I
    */
    ZN := GF(N);
    beta1_N := ChangeRing(beta1, ZN);
    alpha_N := ChangeRing(alpha, ZN);
    rt := Sqrt(ZN!-1);
    Mat_alpha := GetMatrixEndoMod(alpha_N, rt,ZN);
    Mat_u_alpha := (1/Mat_alpha[2][2]) * Mat_alpha;
    // We look for beta2 = Cj + Dk such that beta1 * beta2 in Oalpha mod NO. Using the matrix representation of O/NO, we obtain C * Mat(beta1*j) + D * Mat(beta1*k) - x * Matrix([-u,-1,0,0]) = Vector([0,0,u,1]).
    Mat_beta1_j := GetMatrixEndoMod(Multiply(beta1_N, Vector([ZN!0,0,1,0])), rt, ZN);
    Mat_beta1_k := GetMatrixEndoMod(Multiply(beta1_N, Vector([ZN!0,0,0,1])), rt, ZN);
    // We solve the system above (in C, D and x)
    Row1 := Matrix(1,4,Eltseq(Mat_beta1_j));
    Row2 := Matrix(1,4,Eltseq(Mat_beta1_k));
    Row3 := Matrix(1,4,[-Mat_u_alpha[2][1], -1, 0, 0]);
    MatSolve := VerticalJoin([Row1,Row2,Row3]);
    _, T := EchelonForm(Transpose(MatSolve));
    sol := Vector([0, 0, Mat_u_alpha[2][1], 1])*Transpose(T);
    return Vector([0,0,Integers()!sol[1], Integers()!sol[2]]);
    // this is a test
    //beta2_N := Vector([0,0,sol[1], sol[2]]);
    //Mat_alpha_tilde := Matrix(2,2,[sol[3] * Mat_u_alpha[2][1], sol[3], Mat_u_alpha[2][1], 1]);
    //// assert beta1*beta2 appartient à O \alpha (mod N O)
    //assert Mat_alpha_tilde eq GetMatrixEndoMod(Multiply(beta1_N,beta2_N), rt, ZN);
end function;

function SecondQuaternionApproximation(beta2, S2, p, N);
    /*
    INPUT:
    * beta2 an element of an ideal I of O
    * S2 a powersmooth integer
    * p the prime defining H_{-q, -p}
    * N the norm of I
    OUTPUT:
    * an element beta2prime such that beta2prime = lambda *beta2 mod NO
    */
    C := beta2[3];
    D := beta2[4];
    
    ZN := GF(N);
    r := 3;
    while not(IsSquare(ZN!(S2*r/((C^2+D^2)*p)))) do
	r := NextPrime(r);
    end while;
    S2 := S2 * r;
    lambda := Integers()!Sqrt(ZN!S2/((C^2+D^2)*p));
    assert ZN!(p*lambda^2*(C^2+D^2)) eq ZN!S2;
    // (c,d) satisfies c * C + d * D = (S2 - p * lambda^2 *(C^2+D^2)) / 2 * lambda * p * N
    ZN2 := Integers(N^2);
    repeat
	c := Integers()!Random(ZN);
	d := Integers()!(ZN2!(S2 - p*lambda^2 * (C^2+D^2) - 2*lambda*N*C*c*p)/ZN2!(2*lambda*D*p))/N;
	RHS := (S2 - p * ((lambda *C + c*N)^2 +(lambda * D + d*N)^2))/(N^2);
	RHS := Integers()!RHS;
    until RHS gt -1 and RHS mod 4 eq 1 and IsProbablyPrime(RHS);
    //sufficient condition so that a²+b² = RHS has a solution
    s,a,b := NormEquation(1, RHS);
    return Vector([N*a,N*b,lambda*C+c*N,lambda*D+d*N]);
    // this is just a test
    //assert Norm(beta2prime) eq S2;
    //assert GetMatrixEndoMod(beta2prime, Sqrtm1_modN, ZN) eq lambda * GetMatrixEndoMod(beta2, Sqrtm1_modN, ZN);
end function;

function EquivalentIdeal(I, O: m := Floor(Log(2, p)) + 1, S1 := 3^10, S2 := 3^(3*Floor(Log(2,p)) * Floor(Log(2, Log(2, p)))))
    /*
    INPUT:
    * I an ideal of an order O
    * O an order
    OPTIONAL INPUT:
    * m a bound for the randomization
    * S1 a powersmooth integer
    * S2 a powersmooth integer
    OUTPUT:
    * an ideal J equivalent to I
    * an element w such that J = I * w
    */
    // compute the KLPT algorithm
    NormI := Integers()!IdealNorm(I, O);
    Iprime, delta := PrimeNormEquivalentIdeal(I,O: m:=m);
    N := Integers()!IdealNorm(Iprime, O);
    alpha := IdealSecondGenerator(Iprime, N: m:=m); 
    beta1 := FindQuaternionOfNorm(N*S1, p);
    beta2 := ApproximationInIdeal(N, beta1, alpha);
    beta2prime := SecondQuaternionApproximation(beta2, S2, p, N);
    beta := Multiply(beta1, beta2prime);
    //this is a test
    //assert Integers()!Norm(beta) mod N eq 0 and (Integers()!Norm(beta) div N) mod S2 eq 0;
    //assert In(beta, Iprime);
    return IdealMultiple(Iprime, Conjugate(beta)/N), Multiply(Conjugate(delta),Conjugate(beta))/(NormI * N);
    //this is a test (Say J is the output of the function)
    //NormJSq := IdealNorm(J, O:squared:=true);
    //assert NormJSq mod S2^2 eq 0;
end function;
