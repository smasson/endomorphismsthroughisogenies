// Example of isogeny computation from the connecting ideal.
p := 2^22 * 3^15 - 1;
q := 1;

load "src/Isogeny.m";
load "src/Quaternion.m";
load "src/ZModule.m";
load "src/PointsEquation.m";
load "src/OrdersIdeals.m";

// O is a maximal order of H_{-q,-p}
O := Matrix(Rationals(), 4, 4, [1,0,0,0, 0,1,0,0, 1/2,0,1/2,0, 0,1/2,0,1/2]);

// I is a O-left ideal of norm 3
I := Matrix(Rationals(), 4, 4, [1/2,0,1/2,0, 0,1/2,0,1/2, 0,0,3,0, 0,0,0,3]);
assert IdealNorm(I,O) eq 3;
// I = O.3 + O.alpha
alpha := Vector([29/2, 21, 293/2, 141]);
// (another?) alpha can be obtain using `alpha := IdealSecondGenerator(I, 3);`

// We look for the corresponding isogeny (of degree 3)
// The maximal order O is well-known: it corresponds to End(E) where E:y^2=x^3-x when p = 3 mod 4.
Fp := GF(p);
Fpx<x> := PolynomialRing(Fp);
Fp2<i>  := ext<Fp | x^2+1>;
Fp2y<y> := PolynomialRing(Fp2);
E := EllipticCurve([Fp2!-1, Fp2!0]);

// Its kernel is a point P of order 3
// E[3] = <A, A'>
A := E![60172068522527*i,48359098373596+i*11824579652131];
Aprime := EvaluateQuaternion(Vector([0,0,1,0]), A: OrderP:=3);
// A and Aprime can be obtained using `A, Aprime := TorsionBasis(E, 3);`

// P = n1 * A + n2 * A'
// We find n1 and n2 by solving alpha(P) = 0 \iff n1*alpha(A) + n2*alpha(A') = 0
alphaA := EvaluateQuaternion(alpha, A);
alphaAprime := EvaluateQuaternion(alpha, Aprime);
s := SolveEq([alphaA, alphaAprime]);
assert IsZero(Integers()!s[1] * alphaA + Integers()!s[2] * alphaAprime);
P := Integers()!s[1]*A+Integers()!s[2]*Aprime;
assert Order(P) eq 3;

// phi is the isogeny of kernel P
Eprime, phi := IsogenyFromPoint(P);

/////////
//print//
/////////

function str_quat(q)
    s := "";
    d := Lcm([Denominator(x): x in Eltseq(q)]);
    if d ne 1 then
	s cat:= "\\frac{";
    end if;
    if d*q[1] eq 1 then
	s cat:= "\\bfone";
    else
	s cat:= IntegerToString(Integers()!(d*q[1]));
    end if;
    s cat:= "+";
    s cat:= IntegerToString(Integers()!(d*q[2]));
    s cat:= "\\bfi +";
    s cat:= IntegerToString(Integers()!(d*q[3]));
    s cat:= "\\bfj +";
    s cat:= IntegerToString(Integers()!(d*q[4]));
    s cat:= "\\bfk";
    if d ne 1 then
	s cat:= "}{";
	s cat:= IntegerToString(d);
	s cat:= "}";
    end if;
    return s;
end function;

ss := str_quat(alpha);
print "$\\alpha = ", ss, "$";
n1,n2 :=    InBase(alphaA, A, Aprime, 3);
n1p,n2p :=  InBase(alphaAprime, A, Aprime, 3);
print "$$\\alpha(A) = ", n1, "A + ", n2, "A' \\qquad \\alpha(A') = ", n1p, "A + ", n2p, "A' \\qquad (n,n') \\begin{pmatrix}", n1, " & ", n2, "\\\\", n1p, " & ", n2p, "\\end{pmatrix} = (0,0)$$";
print s, "is a solution.";
print phi;

