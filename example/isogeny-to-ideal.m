// Example of ideal computation from an isogeny.
p := 2^22 * 3^15 - 1;
q := 1;

load "src/Isogeny.m";
load "src/Quaternion.m";
load "src/ZModule.m";
load "src/PointsEquation.m";
load "src/OrdersIdeals.m";

Fp := GF(p);
Fpx<x> := PolynomialRing(Fp);
Fp2<i>  := ext<Fp | x^2+1>;
Fp2y<y> := PolynomialRing(Fp2);
E := EllipticCurve([Fp2!-1, Fp2!0]);
assert IsSupersingular(E);
// E[3] = \langle A, A'\rangle
A := E![60172068522527*i,48359098373596+i*11824579652131];
Aprime := EvaluateQuaternion(Vector([0,0,1,0]), A: OrderP:=3);
// (another?) basis can be obtained using `A, Aprime := TorsionBasis(E, 3);`

// phi is an isogeny of degree 3, of kernel \langle P\rangle
P := 2*A+Aprime;
// A random P can be obtained using `P := Random(0,2) * A + Random(0,2) * Aprime;`
Eprime, phi := IsogenyFromPoint(P);

// We look for an ideal connecting End(E) and End(E')
// The curve E is well-known: End(E) is the following maximal order of H_{-q,-p}
O := Matrix(Rationals(), 4, 4, [1,0,0,0, 0,1,0,0, 1/2,0,1/2,0, 0,1/2,0,1/2]);

// The ideal of norm 3 we look for can be written O.3 + O.alpha for an unknown endomorphism alpha.
// alpha = n1*b1 + ... + n4*b4, where the O = Z<b1,...,b4>
for r in Rows(O) do
    rP := EvaluateQuaternion(r, P);
    if not(IsZero(rP)) then
	n1, n2 := InBase(rP, A, Aprime, 3);
	printf "n_i(%oA+%oA')+",n1,n2;
    end if;
end for;
print "";

S := SolveEq([EvaluateQuaternion(r, P) : r in Rows(O)]);
alpha := Vector(ChangeRing(ChangeRing(S, Integers()),Rationals()) * O);
M := Matrix(Rationals(), 4, 4, [b*3 : b in Rows(O)]);
N := Matrix(Rationals(),4, 4, [Multiply(b, alpha) : b in Rows(O)]);
MN := VerticalJoin(M,N);
print MN;
H, T := RationalHNF(MN);
assert RowSubmatrix(H, 5, 4) eq 0;
I := RowSubmatrix(H, 4);
print I;
