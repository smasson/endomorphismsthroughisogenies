// Computation of equivalent ideals using the KLPT algorithm.

// size 1
//p := 2^8*53 - 1
//size 2
n := 89;
p := 2^n - 1;
//size 3
//p := 2^521-1;


//p = 3 mod 4 so we choose q = 1.
q := 1;

assert IsPrime(p);
Fp := GF(p);
Fpx<x> := PolynomialRing(Fp);
Fp2<u>  := ext<Fp | x^2+1>;
Fp2y<y> := PolynomialRing(Fp2);

load "src/Isogeny.m";
load "src/Quaternion.m";
load "src/ZModule.m";
load "src/PointsEquation.m";
load "src/OrdersIdeals.m";

b1 := [1,   0,    0,   0];
b2 := [0,   1,    0,   0];
b3 := [1/2, 0,  1/2,   0];
b4 := [0, 1/2,    0, 1/2];

w := Fp2!(u);
assert w^2 eq -1;

E := EllipticCurve([Fp2!-1, Fp2!0]);
O := Matrix(Rationals(), 4, 4, [b1,b2,b3,b4]);

ell := 2^(n-2);
assert (p+1) mod ell eq 0;
repeat P := ((p+1) div ell) * Random(E); until Order(P) eq ell;
//Eprime, phi := IsogenyFromPoint(P : OrderP := ell);
printf "--------------------\n      Ideal I\n--------------------\n";
alpha := GetIdealEndomorphism(P, O);
assert Integers()!Norm(alpha) mod ell eq 0;
I := GetIdeal(P, alpha, O, "left");
assert IdealNorm(I, O) eq ell;
print I;
print "";
O_LI, _ := RationalHNF(ComputeOrder(I, "left"));
O_RI, _ := RationalHNF(ComputeOrder(I, "right"));
print "O_L(I)";
print O_LI;
print "O_R(I)";
print O_RI;
print "\n\n";

// Computation of a powersmooth norm ideal J equivalent to I
O0 := O;
BoundS1 := Ceiling(p*Log(2,p));
S1 := 3;
while S1 lt BoundS1 do
    S1 := 3*S1;
end while;

m := Floor(Log(2,p)) + 1;
NormI := Integers()!IdealNorm(I, O0);
Iprime, delta := PrimeNormEquivalentIdeal(I,O0: m:=m);
N := Integers()!IdealNorm(Iprime, O0);
print "Prime norm equivalent ideal found. Log(norm) ~", Floor(Log(2, N)), "(expected ", Floor(Log(2,p)/2), ").";
alpha := IdealSecondGenerator(Iprime, N: m:=m); 
beta1 := FindQuaternionOfNorm(N*S1, p);
print "Beta1 found. Log(norm) ~", Floor(Log(2,Norm(beta1))), "(expected ", Floor(Log(2, p)), ")";
beta2 := ApproximationInIdeal(N, beta1, alpha);

//BoundS2 := 2^50* Ceiling(p^3*Log(2,p));
BoundS2 := 2 * p * N^4;
S2 := 3;
while S2 lt BoundS2 do
    S2 := 3*S2;
end while;

beta2prime := SecondQuaternionApproximation(beta2, S2, p, N);
print "Beta2' found. Log(norm) ~", Floor(Log(2,Norm(beta2prime))), "(expected ", Floor(3*Log(2,p)), ")";
beta := Multiply(beta1, beta2prime);
//this is a test
//assert Integers()!Norm(beta) mod N eq 0 and (Integers()!Norm(beta) div N) mod S2 eq 0;
//assert In(beta, Iprime);
J := IdealMultiple(Iprime, Conjugate(beta)/N);
klpt := Multiply(Conjugate(delta),Conjugate(beta))/(NormI * N);
assert J eq Matrix(Rationals(), 4,4,[Multiply(b, klpt) : b in Rows(I)]);
printf "--------------------\n      Ideal J\n--------------------\n";
O_RJ, _ := RationalHNF(ComputeOrder(J, "right"));
IsomO_RI, _ := RationalHNF(Matrix(Rationals(), 4, 4, [Multiply(Multiply(Inverse(klpt), b), klpt) : b in Rows(O_RI)]));
assert IsomO_RI eq O_RJ;
print "N(J) = ", Factorization(Integers()!IdealNorm(J, O));
print "Log(N(J)) ~", Floor(Log(2,Integers()!IdealNorm(J,O))), "(expected ", Floor(3.5*Log(2,p)), ")";


/*
print "Ideal J";
J, klpt := EquivalentIdeal(I, O);
assert J eq Matrix(Rationals(), 4,4,[Multiply(b, klpt) : b in Rows(I)]);
O_RJ, _ := RationalHNF(ComputeOrder(J, "right"));
IsomO_RI, _ := RationalHNF(Matrix(Rationals(), 4, 4, [Multiply(Multiply(Inverse(klpt), b), klpt) : b in Rows(O_RI)]));
assert IsomO_RI eq O_RJ;
*/


/////////
//print//
/////////

function str_quat(q)
    s := "";
    d := Lcm([Denominator(x): x in Eltseq(q)]);
    if d ne 1 then
	s cat:= "\\frac{";
    end if;
    if d*q[1] eq 1 then
	s cat:= "\\bfone";
    else
	s cat:= IntegerToString(Integers()!(d*q[1]));
    end if;
    s cat:= "+";
    s cat:= IntegerToString(Integers()!(d*q[2]));
    s cat:= "\\bfi +";
    s cat:= IntegerToString(Integers()!(d*q[3]));
    s cat:= "\\bfj +";
    s cat:= IntegerToString(Integers()!(d*q[4]));
    s cat:= "\\bfk";
    if d ne 1 then
	s cat:= "}{";
	s cat:= IntegerToString(d);
	s cat:= "}";
    end if;
    return s;
end function;

