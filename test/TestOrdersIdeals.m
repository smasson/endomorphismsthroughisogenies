load("src/OrdersIdeals.m");

/*
Evaluate_i(P) check that i^2 = -q

Evaluate_j(P) check that j^2 = -p

EvaluateQuaternion(v, P : ListOfIsogenies := [],...) check that for a norm(v) order point, it is zero.

Discriminant(O) check that it is -p² for the known endo rings, checks the examples with conductors (cf thesis)?

IdealMultiplication(L) check norm multiplicativity ? check left and right orders?

IdealMultiple(I, alpha: side:= "right") check left and right order are isomorphic

IdealSecondGenerator(I, N: m := Floor(Log(2,p)) + 1) check that Oa+Ob is equal to I with HNF etc.

GetIdealEndomorphism(P, O:...) check that alpha(P) = 0, that alpha in O ?

GetIdeal(P, alpha, O, side : OrderP := 0) check left order eq O, rightorder is maximal (?) and Norm et Order(P).

ComputeOrder(I, side) check maximality (?)

ConjugateIdeal(I) check same norm, I * BarI = ?

IdealNorm(I, O) check it is an integer (for an integral ideal ?)

GetIsogeny(I, E) check the degreee is the norm, and what else ?

GetIdealFromOrders(O1, O2) check left and right order,...

GetMatrixEndoMod(a, r, F)
GetQuaternionMod(M, r)       check that they are reciprocal ?

PrimeNormEquivalentIdeal(I,O0: m := Floor(Log(2, p)) + 1) check norm is prime, it is an integral ideal ?

FindQuaternionOfNorm(n, p) check norm is ok

ApproximationInIdeal(N, beta1, alpha) need work ?
SecondQuaternionApproximation(beta2, S2, p, N) need work?

EquivalentIdeal(I, O:...) find the w s.t. J = Iw, and left and right order are isomorphic with w ... w^-1.

*/
