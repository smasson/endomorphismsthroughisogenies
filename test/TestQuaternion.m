p := NextPrime(Random(2^60,2^61));

// Choose q as in the Pizer proposition
if p mod 4 eq 3 then
    q := 1;
else if p mod 8 eq 5 then
	q := 2;
    else
	q := 3;
	while q mod 4 ne 3 or LegendreSymbol(p,q) ne -1 do
	    q := NextPrime(q);
	end while;
    end if;
end if;

load "src/Quaternion.m";
load "src/ZModule.m";

function RandomRational( : m:=100);
    return Random(1,m) / Random(1,m);
end function;

printf "Test of `Norm`: ";
for i in [1..100] do
    alpha := Vector([RandomRational() : _ in [1..4]]);
    if Norm(alpha) ne alpha[1]^2 + q * alpha[2]^2 + p * alpha[3]^2 + p*q*alpha[4]^2 then
	printf "NOK\n";
	break;
    end if;
end for;
printf "OK\n";

printf "Test of `Inverse`: ";
for i in [1..100] do
    alpha := Vector([RandomRational() : _ in [1..4]]);
    if Multiply(Inverse(alpha), alpha) ne Vector([Rationals()!1,0,0,0]) then
	printf "NOK\n";
    end if;
end for;
printf "OK\n";

/*
tests pour Multiply(A,B) et Conjugate(A) ?
*/
